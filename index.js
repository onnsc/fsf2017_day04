//load libraries

var express = require("express");
var path = require("path");

var app = express();

//Define routes
// /employee/1   /employee/2  ---> :empNo where : means any and we call this portion empNo
app.get("/employee/:empNo/:infoType", function(req,resp){
    var empNo = req.params.empNo;
    var infoType = req.params.infoType;


//how to find the content type of the request?
//many ways, such as Accept, which is the key on HTML for content type
//a shorter way to do this is res.format

//run this on ARC chrome addon > Request > Header Form > HTTP Headers xxx

    resp.status(200)
        .type(req.get("Accept"));

    switch(req.get("Accept").toLowerCase()){
    
        default:
        case"text/plain":
            resp.send("Name: fred, email: fred@gmail.com, empNo:" + empNo);
            break;

        case"application.json":
            resp.json({
            name: "Fred",
            email: "fred@gmail.com",
            empNo: empNo
        });
            break;
    }
    
    resp.type("text/html")
        .status(200)
       // .send("<h3>Employee Number "+ empNo + "details for" + infoType + "</h3>")  
        //response must be send and only have one send at the end of the line
        //.send(), .sendfile() or .json() for a different format
        //if no data to send back, use resp.end()
        //they are essential to commit the response to tell the browser that the matching is done
        .json({
            name: "Fred",
            email: "fred@gmail.com",
            empNo: empNo
        });
})

app.get("/", function(req,resp){
    
    //Get the user agent
    var userAgent = req.get("user-agent");
    var responseMessage = "<h2>You are using <code>" + userAgent + "</code></h2>";

    //Send the response back
    resp.type("text/html");
    resp.status(200);
    resp.send(responseMessage);

});


app.set("port", process.env.APP_PORT || 3000);

app.listen(app.get("port"),function(){
    console.log("Application is connected to the port at %s on port %d", new Date(), app.get("port"));
});